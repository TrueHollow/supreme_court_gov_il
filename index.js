require('dotenv').config();
const logger = require('./logger')('index.js');
const Parser = require('./service/Parser');
const mongoose = require('./service/db');
const config = require('./config');

logger.info('Script is started');

const main = async () => {
  const parser = new Parser(config.Parser);
  return parser.run();
};

main().then(() => {
  logger.info('Script is finished.');
  mongoose.connection.close();
});
