const mongoose = require('mongoose');
const config = require('../../config');
const logger = require('../../logger')('src/db/index.js');

const mongooseConfig = config.mongoose;
mongoose.connect(mongooseConfig.uri, mongooseConfig.options);

const db = mongoose.connection;
db.on('error', (err) => {
  logger.error('Mongoose error:', err);
});
db.once('open', () => {
  logger.info('Connected to mongo.');
});

require('./models/Data')(mongoose);

module.exports = mongoose;
