module.exports = mongoose => {
  const { Schema } = mongoose;
  const dataSchema = new Schema({
    Id: { type: Number, index: true },
    CaseId: { type: Number, index: true },
    VerdictDt: Date, // '/Date(1562014800000)/'
    CreatedDate: Date, // '/Date(1562044733610)/'
    VerdictsDtString: String,
    CaseNum: Number,
    CaseDesc: String,
    Pages: Number,
    Path: String,
    CaseName: String,
    FileName: String,
    DocName: String,
    Content: {},
    Year: Number,
    TypeCode: Number,
    Type: String,
    CaseDt: {},
    MadorDesc: {},
    SeqNum: Number,
    Technical: Boolean,
    FolderPath: String,
    PageVolume: {},
    DescVolume: {},
    CodeVolume: Number,
    liked: Boolean,
    InyanId: Number,
    QuoteYear: Number,
    CodeQuote: {},
    CodeTitle: {},
    VerdictDesc: {},
    DocNumber: {},

    htmlContent: String,
    deleted: { type: Boolean, default: false },
  });

  mongoose.model('Data', dataSchema);
};
