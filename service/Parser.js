const Net = require('./Net');
const mongoose = require('./db');
const logger = require('../logger')('service/Net.js');

const Data = mongoose.model('Data');

const REGEX_DATES = /\d+/;

function addDays(date, days) {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
}

function dateFromObject(obj) {
  return new Date(obj.year, obj.month - 1, obj.day);
}

class Parser {
  constructor(config) {
    if (!config) {
      throw new Error('Configuration required');
    }
    this.config = config;
  }

  async processResult(data) {
    let validatedData = data.map(item => {
      const result = item;
      if (REGEX_DATES.test(result.VerdictDt)) {
        result.VerdictDt = new Date(
          Number.parseInt(result.VerdictDt.match(REGEX_DATES)[0], 10)
        );
      } else {
        logger.warn('VerdictDt: %s', result.VerdictDt);
        result.VerdictDt = null;
      }
      if (REGEX_DATES.test(result.CreatedDate)) {
        result.CreatedDate = new Date(
          Number.parseInt(result.CreatedDate.match(REGEX_DATES)[0], 10)
        );
      } else {
        logger.warn('VerdictDt: %s', result.CreatedDate);
        result.CreatedDate = null;
      }
      return result;
    });

    if (this.config.checkLocalData) {
      const promises = validatedData.map(async item => {
        const existInDb = await Data.findOne({ Id: item.Id }, '_id');
        if (existInDb) {
          return null;
        }
        return item;
      });
      const dataWithNull = await Promise.all(promises);
      validatedData = dataWithNull.filter(item => item);
    }
    return validatedData;
  }

  async getHtmlForItems(data) {
    const localArray = data.slice();
    const { requestLimit } = this.config;
    do {
      const iteration = localArray.splice(0, requestLimit);
      // eslint-disable-next-line no-await-in-loop
      const htmlArray = await Promise.all(
        iteration.map(item => Net.GetHtmlPage(item))
      );
      htmlArray.forEach((value, index) => {
        iteration[index].htmlContent = value;
      });
    } while (localArray.length);
    return data;
  }

  async getCases(startDate, endDate) {
    let lStartDate = addDays(endDate, -1);
    while (lStartDate >= startDate) {
      const lEndDate = addDays(lStartDate, 1);
      logger.debug(
        'Getting data from %s to %s',
        lStartDate.toJSON(),
        lEndDate.toJSON()
      );
      // eslint-disable-next-line no-await-in-loop
      const json = await Net.GetData(lStartDate, lEndDate);
      const { data } = json;
      if (data) {
        logger.debug('Received %d object.', data.length);
        // eslint-disable-next-line no-await-in-loop
        const validatedData = await this.processResult(data);
        // eslint-disable-next-line no-await-in-loop
        const dataWithHtml = await this.getHtmlForItems(validatedData);
        logger.debug('Saving %d data objects', dataWithHtml.length);
        // eslint-disable-next-line no-await-in-loop
        await Data.insertMany(dataWithHtml);
      } else {
        logger.warn('No data.');
      }
      lStartDate = addDays(lStartDate, -1);
    }
  }

  async run() {
    const startDate = dateFromObject(this.config.startDate);
    const endDate = dateFromObject(this.config.endDate);
    await this.getCases(startDate, endDate);
  }
}

module.exports = Parser;
