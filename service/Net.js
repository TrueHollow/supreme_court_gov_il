const request = require('request');
const iconv = require('iconv-lite');
const logger = require('../logger')('service/Net.js');

const HEADERS = {
  'User-Agent':
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
  Referer: 'https://supremedecisions.court.gov.il/Verdicts/Search/1',
};

const SUPREME_COURT_URL =
  'https://supremedecisions.court.gov.il/Home/SearchVerdicts';

const REQUEST_OBJECT = {
  document: {
    AllSubjects: [{ Subject: null, SubSubject: null, SubSubSubject: null }],
    CaseNum: null,
    Category1: null,
    Category3: null,
    CodeCategory3: [],
    CodeInyan: [],
    CodeJudges: [],
    CodeMador: [],
    CodeSub2: [],
    CodeTypes: [],
    Counsel: [
      {
        Inverted: false,
        MatchOrder: false,
        NearDistance: 3,
        option: '2',
        Synonym: false,
        Text: '',
        textOperator: 2,
      },
    ],
    dateType: 2,
    fromPages: null,
    Inyan: null,
    Judges: null,
    JudgesOperator: 2,
    Judgment: null,
    LastCourtCaseNum: null,
    LastCourtsMonth: null,
    LastCourtsYear: null,
    LastInyan: null,
    Mador: null,
    Old: false,
    Parties: [
      {
        Inverted: false,
        MatchOrder: false,
        NearDistance: 3,
        option: '2',
        Synonym: false,
        Text: '',
        textOperator: 2,
      },
    ],
    publishDate: 8,
    PublishFrom: '2019-06-30T14:00:00.000Z',
    PublishTo: '2019-07-01T14:00:00.000Z',
    SearchText: [
      {
        Inverted: false,
        MatchOrder: false,
        NearDistance: 3,
        option: '2',
        Synonym: false,
        Text: '',
        textOperator: 1,
      },
    ],
    Subjects: null,
    SubSubjects: null,
    SubSubSubjects: null,
    Technical: null,
    TerrestrialCourts: null,
    toPages: null,
    translationDateType: 1,
    translationPublishDate: 8,
    translationPublishFrom: '2019-07-17T04:15:12.373Z',
    translationPublishTo: '2019-08-17T04:15:12.373Z',
    Type: null,
    TypeCourts: null,
    TypeCourts1: null,
    Volume: null,
    Year: null,
  },
  lan: '1',
};

const HTTP_OK = 200;
const HTTP_NOT_FOUND = 404;

const delay = async (timeout = 5000) => {
  return new Promise(resolve => {
    setTimeout(resolve, timeout);
  });
};

const DEFAULT_ENCODING = 'windows-1255';
const REGEX_CHARSET = /Charset=["']?([\w-]+)["']?/i;

const GetRequest = async url => {
  return new Promise((resolve, reject) => {
    request(
      {
        url,
        method: 'GET',
        headers: HEADERS,
        timeout: 20000,
        encoding: null,
      },
      (err, response, buffer) => {
        if (err) {
          return reject(err);
        }
        if (
          response.statusCode !== HTTP_OK &&
          response.statusCode !== HTTP_NOT_FOUND
        ) {
          return reject(new Error(`Response code: ${response.statusCode}`));
        }
        let encoding = DEFAULT_ENCODING;
        let html = iconv.decode(buffer, encoding);
        const match = html.match(REGEX_CHARSET);
        if (match && match[1] !== encoding) {
          [, encoding] = match;
          logger.warn(`Detected unusual encoding (${encoding})`);
          html = iconv.decode(buffer, encoding);
        }
        if (!match) {
          logger.warn(
            `html file doesn't have charset. using default encoding.`
          );
        }
        return resolve(html.replace(encoding, 'utf-8'));
      }
    );
  });
};

const PostRequest = async (url, obj) => {
  return new Promise((resolve, reject) => {
    request(
      {
        url,
        method: 'POST',
        headers: HEADERS,
        timeout: 20000,
        body: obj,
        json: true,
      },
      (err, incomingMessage, body) => {
        if (err) {
          return reject(err);
        }
        if (incomingMessage.statusCode !== HTTP_OK) {
          return reject(
            new Error(`Response code: ${incomingMessage.statusCode}`)
          );
        }
        return resolve(body);
      }
    );
  });
};

class Net {
  /**
   * Get json cases data
   * @param startDate
   * @param endDate
   * @return {Promise<Object>}
   */
  static async GetData(startDate, endDate) {
    const obj = { ...REQUEST_OBJECT };
    obj.document.PublishFrom = startDate.toJSON();
    obj.document.PublishTo = endDate.toJSON();
    let json;
    do {
      try {
        logger.debug('POST request (%s).', SUPREME_COURT_URL);
        // eslint-disable-next-line no-await-in-loop
        json = await PostRequest(SUPREME_COURT_URL, obj);
      } catch (e) {
        logger.error(e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restarting request.');
      }
    } while (!json);
    return json;
  }

  static async GetHtmlPage(item) {
    if (!item.Path || !item.FileName) {
      logger.warn(`Item doesn't have 'Path' or 'FileName' (${item})`);
      return null;
    }
    let html;
    // https://supremedecisions.court.gov.il/Home/Download?path=HebrewVerdicts\19\560\024\r04&fileName=19024560.R04&type=2
    // https://supremedecisions.court.gov.il/Home/Download?path=HebrewVerdicts|19|700|001|z01&fileName=19001700.Z01&type=2
    const url = `https://supremedecisions.court.gov.il/Home/Download?path=${item.Path}&fileName=${item.FileName}&type=2`;
    do {
      try {
        logger.debug(`Getting html content ('${url}').`);
        // eslint-disable-next-line no-await-in-loop
        html = await GetRequest(url);
      } catch (e) {
        logger.error(e);
        // eslint-disable-next-line no-await-in-loop
        await delay();
        logger.debug('Restarting request.');
      }
    } while (!html);
    return html;
  }
}

module.exports = Net;
